<%@ page isELIgnored="false"%>
<%@ page import="shop.Product" %><%--
  Created by IntelliJ IDEA.
  User: gc16077
  Date: 03/03/2018
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="shop.ShopDB" %>
<%@ page import="java.util.Collection" %>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/searchValidation.js"></script>
    <link rel="stylesheet" type="text/css" href="css/content.css" />
    <title>search products</title>

</head>
<body>
<jsp:include page="header.jsp"/>
<jsp:include page="sideBar.jsp"/>
<div id="content">

    <!-- Form used to do the searching of products -->
    <h2>Search for your product by name here</h2>
    <form action="searchProducts.jsp" id="search-prod-form">
        <input type="text" name="productName" id="searchText">
        <input type="button" value="search" id="searchBut">
    </form>

    <!-- Java Bean that houses all the DB searching methods and logic -->
    <jsp:useBean id="searchBean" class="shop.beans.SearchProd">
        <jsp:setProperty name="searchBean" property="searchQuery" value='${param["productName"]}'/>
    </jsp:useBean>


    <%
        searchBean.setProd( searchBean.getSingleProductByName(searchBean.getSearchQuery()));
        if(searchBean.getProd() != null){
    %>
        <div id="product-box">
        <div align="center">
            <h2> <%=searchBean.getProd().title%>  by <%=searchBean.getProd().artist %> </h2>
            <img src="<%=searchBean.getProd().fullimage%>" width="200px"height="200px" />
            <p> <%= searchBean.getProd().description %> </p>
            <p> <a href="<%="basket.jsp?addItem=" + searchBean.getProd().PID%> ">add to basket</a> </p>
        </div>
        </div>

    <h2>Products also created by <%=searchBean.getProd().artist%>:</h2>
    <div id="product-table">
        <table>
            <tr>
                <th> Title </th> <th> Price </th> <th> Picture </th>
            </tr>
            <%
                for (Product pro : searchBean.getProductsByArtist(searchBean.getProd().artist)) {
            %>
            <tr>
                <td> <%= pro.title %> </td>
                <td> <%= pro.price %> </td>
                <td> <a href = '<%="viewProduct.jsp?pid="+pro.PID%>'> <img src="<%= pro.thumbnail %>" width="25" height="25"/> </a> </td>
            </tr>
            <%
                }
            %>
        </table>
    </div>
    <%
        }

        else if(searchBean.getProd()== null){
    %>
      <h2>Couldn't find your query!</h2>
    <h2> Hopefully below we found some similar matches</h2>
    <div id="product-table">
        <table>
            <tr>
                <th> Title </th> <th> Price </th> <th> Picture </th>
            </tr>
            <%
                for (Product pro : searchBean.getProductsByTerm(searchBean.getSearchQuery(),"Title")) {
            %>
            <tr>
                <td> <%=pro.title%> </td>
                <td> <%= pro.price %> </td>
                <td> <a href = '<%="viewProduct.jsp?pid="+pro.PID%>'> <img src="<%= pro.thumbnail %>" width="25" height="25"/> </a> </td>
            </tr>
            <%
                }
            %>
        </table>
    </div>
    <%
        }
        else{
    %>
    <h2>No results found!</h2>
    <%
        }
    %>
</div>
</body>
</html>
