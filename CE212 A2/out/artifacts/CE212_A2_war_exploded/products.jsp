<%@ page import="shop.Product"%>
<%@ page import="java.util.ArrayList" %>

<jsp:useBean id='db'
             scope='session'
             class='shop.ShopDB' />


<%
    String filter = request.getParameter("filter");
    if(filter == null){
        filter = "all";
    }
%>
<html>
<head>
<title>My shop</title>

</head>
<body>
<div id="filter-box" style="margin-bottom: 5px;">
    <form action="index.jsp">
    <label>Filter by artist</label>
    <select name="filter">
        <option>all</option>
        <%
            ArrayList<String> productList = db.getArtistName(db.getAllProducts());
            for(String s: productList){%>
        <option><%out.println(s);%></option>
        <%}%>
    </select>
        <button type="submit">filter</button>
    </form>
</div>
<table>
<tr>
<th> Title </th> <th> Price </th> <th> Picture </th>
</tr>
<%
    for (Product product : db.getAllProducts() ) {
        // now use HTML literals to create the table
        // with JSP expressions to include the live data
        // but this page is unfinished - the thumbnail
        // needs a hyperlink to the product description,
        // and there should also be a way of selecting
        // pictures from a particular artist
        if(!filter.equals("all")) {
            if (!product.artist.equals(filter)) continue;
        }
        %>
        <tr>
             <td> <%= product.title %> </td>
             <td> <%= product.price %> </td>
             <td> <a href = '<%="viewProduct.jsp?pid="+product.PID%>'> <img src="<%= product.thumbnail %>" width="25" height="25"/> </a> </td>
        </tr>
        <%
    }
 %>
 </table>
</body>
</html>
