/**
 * Created by gc16077 on 04/03/2018.
 */
$(document).ready(function () {
   $("#searchBut").click(function () {
       var invalidCharArray = ["$","?","!",".",",","-","*","(",")","'","~"];
       var query = $("#searchText").val();
       for(var i=0;i<invalidCharArray.length;i++){
           var check = query.includes(invalidCharArray[i]);
           if(check){
               break;
           }
       }
       if(!check)$("#search-prod-form").submit();
       else alert("Sorry an invalid character was found");

   })
});