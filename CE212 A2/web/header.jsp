<html>
	<!-- Header file that is included in every page  -->
	<head>
		<link rel="stylesheet" type="text/css" href="css\header.css">
		<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet"> 
	</head>
	<body>
		<div id="header">
			<div id="title-border">
			<h1>Scamazon</h1>
			<h4>The number one place to lose your money!</h4>
			</div>
			<div class="nav-bar">
			<div class="nav-button"><a class="but-text" href="index.jsp">Products</a></div> <div class="nav-button"><a class="but-text" href="basket.jsp">Basket</a></div> <div class="nav-button">Checkout</div> <div class="nav-button"><a href="searchProducts.jsp">Search</a></div>
		</div>
		</div>
	
	</body>
</html>