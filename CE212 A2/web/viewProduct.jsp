<%@ page import="shop.Product"%>

<jsp:useBean id='db'
             scope='session'
             class='shop.ShopDB' />
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/content.css" />
<title>My shop</title>
</head>
<body>
<jsp:include page="header.jsp"/>
<jsp:include page="sideBar.jsp"/>
<jsp:useBean id="ProdBean" class="shop.beans.ProductBoxBean">
    <jsp:setProperty name="ProdBean" property="pid" value='<%=request.getParameter("pid")%>'/>
    <jsp:setProperty name="ProdBean" property="product" value="<%=ProdBean.initProduct()%>"/>
</jsp:useBean>
<div id="product-box">
<%
    Product product = ProdBean.getProduct();
    if (product == null) {
        // do something sensible!!!
        out.println( "Please select a product from the products page!" );
    }
    else {
%>
        <div align="center">
        <h2> <%= product.title %>  by <%= product.artist %> </h2>
        <img src="<%= product.fullimage %>" width="200px"height="200px"/>
        <p> <%= product.description %> </p>
        <p> <a href="<%="basket.jsp?addItem=" + product.PID%>">add to basket</a></p>
        </div>
        <%
    }
%>
<p> test? </p>
</div>
</body>
</html>
